<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Product;
use Illuminate\Http\Response;

class ProductController extends Controller
{
    public function index()
    {
        if(request()->ajax())
        {
            return datatables()->of(Product::select('*'))
            ->addColumn('action', 'DataTables.action')
            ->rawColumns(['action'])
            ->addIndexColumn()
            ->make(true);
        }

        return view('list');
    }

    public function store(Request $request)
    {
        $productId = $request->id;
        $product = Product::updateOrCreate(['id' => $productId], ['title' => $request->title, 'product_code' => $request->product_code, 'description' => $request->description]);

        return Response::json($product);
    }

    public function edit($id)
    {
        $where = array('id' => $id);
        $product = Product::where($where)->first();

        return Response::json($product);
    }

    public function destroy($id)
    {
        $product = Product::where('id', $id)->delete();
        return Response::json($product);
    }
}
